//
// Created by darkhead on 28/4/18.
//

#include <iostream>
#include "../headers/Controller.h"
#include "../headers/Mesh.h"
#include "../headers/EngineSetup.h"
#include "../headers/Object3D.h"

Controller::Controller()
{
}

void Controller::handleKeyboard(SDL_Event *event, Camera *camera, bool &done)
{
    switch(event->type)
    {
        case SDL_KEYDOWN:
            switch( event->key.keysym.sym ) {
                // Camera position
                // Y
                case SDLK_h:
                    EngineSetup::getInstance()->showHelp();
                    break;
                case SDLK_q:
                    camera->getPosition()->y += EngineSetup::getInstance()->CONTROLLER_SPEED_MOVEMENT_CAMERA;
                    break;
                case SDLK_a:
                    camera->getPosition()->y -= EngineSetup::getInstance()->CONTROLLER_SPEED_MOVEMENT_CAMERA;
                    break;
                    // X
                case SDLK_o:
                    camera->getPosition()->x -= EngineSetup::getInstance()->CONTROLLER_SPEED_MOVEMENT_CAMERA;
                    break;
                case SDLK_p:
                    camera->getPosition()->x += EngineSetup::getInstance()->CONTROLLER_SPEED_MOVEMENT_CAMERA;
                    break;
                    // Z
                case SDLK_w:
                    camera->getPosition()->z += EngineSetup::getInstance()->CONTROLLER_SPEED_MOVEMENT_CAMERA;
                    break;
                case SDLK_s:
                    camera->getPosition()->z -= EngineSetup::getInstance()->CONTROLLER_SPEED_MOVEMENT_CAMERA;
                    break;
                    // camera rotation
                case SDLK_UP:
                    camera->getRotation()->x += EngineSetup::getInstance()->CONTROLLER_SPEED_ROTATION_CAMERA;
                    break;
                case SDLK_DOWN:
                    camera->getRotation()->x -= EngineSetup::getInstance()->CONTROLLER_SPEED_ROTATION_CAMERA;
                    break;
                case SDLK_LEFT:
                    camera->getRotation()->y -= EngineSetup::getInstance()->CONTROLLER_SPEED_ROTATION_CAMERA;
                    break;
                case SDLK_RIGHT:
                    camera->getRotation()->y += EngineSetup::getInstance()->CONTROLLER_SPEED_ROTATION_CAMERA;
                    break;
                case SDLK_f:
                    camera->getRotation()->z -= EngineSetup::getInstance()->CONTROLLER_SPEED_ROTATION_CAMERA;
                    break;
                case SDLK_g:
                    camera->getRotation()->z += EngineSetup::getInstance()->CONTROLLER_SPEED_ROTATION_CAMERA;
                    break;

                case SDLK_k:
                    camera->horizontal_fov--;
                    printf("Camera HFOV: %f\r\n", camera->horizontal_fov);
                    break;

                case SDLK_l:
                    camera->horizontal_fov++;
                    printf("Camera HFOV: %f\r\n", camera->horizontal_fov);
                    break;

                case SDLK_TAB:
                    if (EngineSetup::getInstance()->TRIANGLE_MODE_TEXTURIZED) {
                        EngineSetup::getInstance()->TRIANGLE_MODE_TEXTURIZED = false;
                        printf("Textures OFF\r\n");
                    } else {
                        EngineSetup::getInstance()->TRIANGLE_MODE_TEXTURIZED = true;
                        printf("Textures ON\r\n");
                    }
                    break;
                case SDLK_RSHIFT:
                    if (EngineSetup::getInstance()->DRAW_FRUSTUM) {
                        EngineSetup::getInstance()->DRAW_FRUSTUM = false;
                        printf("Draw frustum OFF\r\n");
                    } else {
                        EngineSetup::getInstance()->DRAW_FRUSTUM = true;
                        printf("Draw frustum ON\r\n");
                    }
                    break;
                case SDLK_CAPSLOCK:
                    if (EngineSetup::getInstance()->TRIANGLE_MODE_WIREFRAME) {
                        EngineSetup::getInstance()->TRIANGLE_MODE_WIREFRAME = false;
                        printf("Wireframes OFF\r\n");
                    } else {
                        EngineSetup::getInstance()->TRIANGLE_MODE_WIREFRAME = true;
                        printf("Wireframes ON\r\n");
                    }
                    break;
                case SDLK_LSHIFT:
                    if (EngineSetup::getInstance()->TRIANGLE_RENDER_DEPTH_BUFFER) {
                        EngineSetup::getInstance()->TRIANGLE_RENDER_DEPTH_BUFFER = false;
                        printf("EngineBuffers OFF\r\n");
                    } else {
                        EngineSetup::getInstance()->TRIANGLE_RENDER_DEPTH_BUFFER = true;
                        printf("EngineBuffers ON\r\n");
                    }
                    break;
                case SDLK_COMMA:
                    if (EngineSetup::getInstance()->TRIANGLE_FACECULLING) {
                        EngineSetup::getInstance()->TRIANGLE_FACECULLING = false;
                        printf("Triangle Faceculling OFF\r\n");
                    } else {
                        EngineSetup::getInstance()->TRIANGLE_FACECULLING = true;
                        printf("Triangle Faceculling ON\r\n");
                    }
                    break;
                case SDLK_BACKSPACE:
                    if (EngineSetup::getInstance()->TRIANGLE_FRUSTUM_CULLING) {
                        EngineSetup::getInstance()->TRIANGLE_FRUSTUM_CULLING = false;
                        printf("Frustum Culling OFF\r\n");
                    } else {
                        EngineSetup::getInstance()->TRIANGLE_FRUSTUM_CULLING = true;
                        printf("Frustum Culling ON\r\n");
                    }
                    break;

                case SDLK_LALT:
                    if (EngineSetup::getInstance()->MESH_DEBUG_INFO) {
                        EngineSetup::getInstance()->MESH_DEBUG_INFO = false;
                        printf("Mesh Debug info OFF\r\n");
                    } else {
                        EngineSetup::getInstance()->MESH_DEBUG_INFO = true;
                        printf("Mesh Debug info ON\r\n");
                    }
                    break;
                case SDLK_RALT:
                    if (EngineSetup::getInstance()->TRIANGLE_RENDER_CLIPPING) {
                        EngineSetup::getInstance()->TRIANGLE_RENDER_CLIPPING = false;
                        printf("Clipping OFF\r\n");
                    } else {
                        EngineSetup::getInstance()->TRIANGLE_RENDER_CLIPPING = true;
                        printf("Clipping ON\r\n");
                    }
                    break;
                case SDLK_LCTRL:
                    if (EngineSetup::getInstance()->TRIANGLE_MODE_PIXELS) {
                        EngineSetup::getInstance()->TRIANGLE_MODE_PIXELS = false;
                        printf("Draw triangle pixels OFF\r\n");
                    } else {
                        EngineSetup::getInstance()->TRIANGLE_MODE_PIXELS = true;
                        printf("Draw triangle pixels ON\r\n");
                    }
                    break;
                case SDLK_y:
                    if (EngineSetup::getInstance()->RENDER_OBJECTS_AXIS) {
                        EngineSetup::getInstance()->RENDER_OBJECTS_AXIS = false;
                        printf("Render Axis Objects OFF\r\n");
                    } else {
                        EngineSetup::getInstance()->RENDER_OBJECTS_AXIS = true;
                        printf("Render Axis Objects ON\r\n");
                    }
                    break;
                case SDLK_8:
                    if (EngineSetup::getInstance()->TRIANGLE_RENDER_NORMAL) {
                        EngineSetup::getInstance()->TRIANGLE_RENDER_NORMAL = false;
                        printf("Triangle normals OFF\r\n");
                    } else {
                        EngineSetup::getInstance()->TRIANGLE_RENDER_NORMAL = true;
                        printf("Triangle normals ON\r\n");
                    }
                    break;

                case SDLK_QUOTE:
                    if (EngineSetup::getInstance()->DRAW_OBJECT3D_BILLBOARD) {
                        EngineSetup::getInstance()->DRAW_OBJECT3D_BILLBOARD = false;
                        printf("Billboard Object3D OFF\r\n");
                    } else {
                        EngineSetup::getInstance()->DRAW_OBJECT3D_BILLBOARD = true;
                        printf("Billboard Object3D ON\r\n");
                    }
                    break;
                case SDLK_0:
                    if (EngineSetup::getInstance()->SHOW_SHADOWMAPPING_BUFFER) {
                        EngineSetup::getInstance()->SHOW_SHADOWMAPPING_BUFFER = false;
                        printf("SHOW_SHADOWMAPPING_BUFFER OFF\r\n");
                    } else {
                        EngineSetup::getInstance()->SHOW_SHADOWMAPPING_BUFFER = true;
                        printf("SHOW_SHADOWMAPPING_BUFFER ON\r\n");
                    }
                    break;
                case SDLK_ESCAPE:
                    done = true;
                    break;

                case SDL_QUIT:
                    done = true;
                    break;
            }
        break;
    }

}

void Controller::handleKeyboardObject3D(SDL_Event *event, Object3D *object)
{
    SDL_Keycode kc = event->key.keysym.sym;

    if (kc == SDLK_z) {
        object->getRotation()->x -= EngineSetup::getInstance()->CONTROLLER_SPEED_ROTATION;
        std::cout << object->getLabel() << " | ";
        object->getRotation()->consoleInfo("Rotating mesh... ", true);
    }

    if (kc == SDLK_x) {
        object->getRotation()->x += EngineSetup::getInstance()->CONTROLLER_SPEED_ROTATION;
        std::cout << object->getLabel() << " | ";
        object->getRotation()->consoleInfo("Rotating mesh... ", true);
    }

    if (kc == SDLK_c) {
        object->getRotation()->y -= EngineSetup::getInstance()->CONTROLLER_SPEED_ROTATION;
        std::cout << object->getLabel() << " | ";
        object->getRotation()->consoleInfo("Rotating mesh... ", true);
    }
    if (kc == SDLK_v) {
        object->getRotation()->y += EngineSetup::getInstance()->CONTROLLER_SPEED_ROTATION;
        std::cout << object->getLabel() << " | ";
        object->getRotation()->consoleInfo("Rotating mesh... ", true);
    }
    if (kc == SDLK_b) {
        object->getRotation()->z -= EngineSetup::getInstance()->CONTROLLER_SPEED_ROTATION;
        std::cout << object->getLabel() << " | ";
        object->getRotation()->consoleInfo("Rotating mesh... ", true);
    }
    if (kc == SDLK_n) {
        object->getRotation()->z += EngineSetup::getInstance()->CONTROLLER_SPEED_ROTATION;
        std::cout << object->getLabel() << " | ";
        object->getRotation()->consoleInfo("Rotating mesh... ", true);
    }

    // position
    if (kc == SDLK_1) {
        object->getPosition()->y += EngineSetup::getInstance()->CONTROLLER_SPEED_MOVEMENT;
    }
    if (kc == SDLK_2) {
        object->getPosition()->y -= EngineSetup::getInstance()->CONTROLLER_SPEED_MOVEMENT;
    }
    if (kc == SDLK_3) {
        object->getPosition()->x -= EngineSetup::getInstance()->CONTROLLER_SPEED_MOVEMENT;
    }
    if (kc == SDLK_4) {
        object->getPosition()->x += EngineSetup::getInstance()->CONTROLLER_SPEED_MOVEMENT;
    }
    if (kc == SDLK_5) {
        object->getPosition()->z -= EngineSetup::getInstance()->CONTROLLER_SPEED_MOVEMENT;
    }
    if (kc == SDLK_6) {
        object->getPosition()->z+=EngineSetup::getInstance()->CONTROLLER_SPEED_MOVEMENT;
    }

    // scale
    if (kc == SDLK_GREATER) {
        object->scale-=0.01f;
    }
    if (kc == SDLK_m) {
        object->scale+=0.01f;
    }
}