#include "../headers/Engine.h"
#include "../headers/Mesh.h"
#include "../headers/EngineBuffers.h"
#include "../headers/Frustum.h"
#include "../headers/Object3D.h"
#include "../headers/LTimer.h"
#include "../headers/Drawable.h"
#include "../headers/Render.h"
#include "../headers/LightPoint.h"
#include <chrono>
#include <iostream>

using namespace std::chrono;

Engine::Engine()
{
    //The window we'll be rendering to
    window = NULL;

    //The surface contained by the window
    screenSurface = NULL;

    // Init TTF
    InitFontsTTF();

    // used to finish all
    finish = false;

    // gameobjects

    this->gameObjects = new Object3D*[EngineSetup::getInstance()->ENGINE_MAX_GAMEOBJECTS];
    this->numberGameObjects = 0;

    // lights
    this->lightPoints = new LightPoint*[EngineSetup::getInstance()->ENGINE_MAX_GAMEOBJECTS];
    this->numberLightPoints = 0;

    // cam
    cam = new Camera();
    cam->setLabel("Camera");

    // input controller
    cont = new Controller();

}


bool Engine::InitWindow()
{
    //Initialize SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
        return false;
    } else {
        //Create window
        window = SDL_CreateWindow(
                "SDL Tutorial",
                SDL_WINDOWPOS_UNDEFINED,
                SDL_WINDOWPOS_UNDEFINED,
                EngineSetup::getInstance()->SCREEN_WIDTH,
                EngineSetup::getInstance()->SCREEN_HEIGHT,
                SDL_WINDOW_SHOWN
        );

        if (window == NULL) {
            printf("Window could not be created! SDL_Error: %s\n", SDL_GetError());
            return false;
        } else {
            screenSurface = SDL_GetWindowSurface( window );
        }
    }

    return true;
}

void Engine::InitFontsTTF()
{
    std::cout << "Initializating TTF..." << std::endl;
    // global font
    if (TTF_Init() < 0) {
        printf("TTF_OpenFont: %s\n", TTF_GetError());
    } else {
        std::cout << "Loading ../fonts/arial.ttf" << std::endl;
        font = TTF_OpenFont( "../fonts/arial.ttf", 10 );
        if(!font) {
            printf("TTF_OpenFont: %s\n", TTF_GetError());
        }
    }
}

void Engine::run()
{
    this->onStart();
    this->mainLoop();
    this->onEnd();

    Close();
}

void Engine::mainLoop() {
    while(!finish) {
        // start frame
        while (SDL_PollEvent(&e)) {
            this->cameraUpdate();
            this->onUpdateEvent();
        }

        this->onUpdate();
        this->windowUpdate();
    }
}

void Engine::cameraUpdate()
{
    cont->handleKeyboard(&this->e, this->cam, this->finish);
    cam->syncFrustum();
}

void Engine::windowUpdate()
{
    if (EngineSetup::getInstance()->DRAW_FRUSTUM) {
        Drawable::drawFrustum(screenSurface, cam->frustum, cam, true, true, true);
    }

    Drawable::drawMainAxis(screenSurface, cam);

    LightPoint *lp = this->lightPoints[0];
    EngineBuffers::getInstance()->flipVideoBuffer(screenSurface, lp);

    SDL_UpdateWindowSurface( window );
    SDL_FillRect(screenSurface, NULL, SDL_MapRGB(screenSurface->format, 0x00, 0x00, 0x00));
}

void Engine::onStart() {
    cam->setPosition( EngineSetup::getInstance()->CameraPosition );
    cam->setRotation( Vertex(0, 0, 0) );
}

void Engine::onUpdateEvent() {
    // handle Keyboard
    for (int i = 0; i < this->numberGameObjects; i++) {
        if ( this->gameObjects[i]->isHandleKeyboard() ) {
            this->cont->handleKeyboardObject3D(&this->e, this->gameObjects[i]);
        }
    }
    for (int i = 0; i < this->numberLightPoints; i++) {
        this->lightPoints[i]->syncFrustum();
        if ( this->lightPoints[i]->isHandleKeyboard() ) {
            this->cont->handleKeyboardObject3D(&this->e, this->lightPoints[i]);
        }
    }
}

void Engine::onUpdate() {
    EngineBuffers::getInstance()->clearDepthBuffer();
    EngineBuffers::getInstance()->clearVideoBuffer();

    // Clear every light's shadow mapping
    this->clearLightPointsShadowMappings();

    // Fill ShadowMapping throw every object (mesh)
    this->objects3DShadowMapping();

    // render mesh
    this->drawMeshes();

    this->drawLightPointsBillboard();
}

void Engine::clearLightPointsShadowMappings() {
    for (int i = 0; i < this->numberLightPoints; i++) {
        LightPoint *oLight = this->lightPoints[i];
        oLight->clearShadowMappingBuffer();
    }
}

void Engine::objects3DShadowMapping()
{
    for (int i = 0; i < this->numberGameObjects; i++) {
        Mesh *oMesh = dynamic_cast<Mesh*> (this->gameObjects[i]);
        if (oMesh != NULL) {
            for (int j = 0; j < this->numberLightPoints; j++) {
                LightPoint *oLight = this->lightPoints[j];
                oMesh->shadowMapping(oLight);
            }
        }
    }
}

void Engine::drawMeshes()
{
    // draw meshes
    for (int i = 0; i < this->numberGameObjects; i++) {
        Mesh *oMesh = dynamic_cast<Mesh*> (this->gameObjects[i]);
        if (oMesh != NULL) {
            oMesh->draw(screenSurface, cam);
        }
    }
}

void Engine::drawLightPointsBillboard()
{
    for (int i = 0; i < this->numberLightPoints; i++) {
        LightPoint *oLight= this->lightPoints[i];
        if (oLight != NULL) {
            oLight->billboard->updateUnconstrainedQuad( 0.3, 0.3, oLight, cam->upVector(), cam->rightVector() );
            Drawable::drawBillboard(Engine::screenSurface, oLight->billboard, Engine::cam );
            Drawable::drawObject3DAxis(screenSurface, oLight, cam, true, true, true);

        }
    }
}

void Engine::onEnd() {

}

void Engine::addObject3D(Object3D *obj, std::string label) {
    std::cout << "Adding Object3D: '" << label << "'" << std::endl;

    gameObjects[numberGameObjects] = obj;
    gameObjects[numberGameObjects]->setLabel(label);
    numberGameObjects++;
}

void Engine::addLightPoint(LightPoint *lightPoint, std::string label) {
    std::cout << "Adding LightPoint: '" << label << "'" << std::endl;

    lightPoints[numberLightPoints] = lightPoint;
    numberLightPoints++;
}

Object3D* Engine::getObjectByLabel(std::string label) {
    for (int i = 0; i < numberGameObjects; i++ ) {
        if (!gameObjects[i]->getLabel().compare(label)) {
            return gameObjects[i];
        }
    }
}

void Engine::Close()
{
    TTF_CloseFont( font );
    SDL_DestroyWindow( window );
    SDL_Quit();
}
