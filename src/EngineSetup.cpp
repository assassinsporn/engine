
#include <iostream>
#include "../headers/EngineSetup.h"

EngineSetup* EngineSetup::instance = 0;

EngineSetup* EngineSetup::getInstance()
{
    if (instance == 0) {
        instance = new EngineSetup();
    }

    return instance;
}

EngineSetup::EngineSetup() {

}

void EngineSetup::showHelp() {

    std::cout << std::endl;
    std::cout << "RENDER TRIANGLE MODES" << std::endl;
    std::cout << "=====================" << std::endl;
    std::cout << "Triangle vertices: LEFT_CTRL" << std::endl;
    std::cout << "Texture Maping: TAB" << std::endl;
    std::cout << "Texture Wireframes: BLOCK MAYUSC" << std::endl;

    std::cout << std::endl;
    std::cout << "MESH MANIPULATION" << std::endl;
    std::cout << "=====================" << std::endl;
    std::cout << "Move: Y Axis: 1, 2 | X Axis: 3, 4 | Z Axis: 5, 6" << std::endl;
    std::cout << "Rotate Y Axis: z, x | X Axis: color, v | Z Axis: b, n" << std::endl;

    std::cout << std::endl;
    std::cout << "OPTIMIZATION" << std::endl;
    std::cout << "============" << std::endl;
    std::cout << "Back-Face culling: COMMA" << std::endl;
    std::cout << "Only Frustum View: BACKSPACE" << std::endl;
    std::cout << "Frustum Clipping: RIGHT_ALT " << std::endl;
    std::cout << "Z-Buffer: LEFT_SHIFT" << std::endl;

    std::cout << std::endl;
    std::cout << "FOV" << std::endl;
    std::cout << "=====================" << std::endl;
    std::cout << "Decrease Horizontal FOV: k" << std::endl;
    std::cout << "Increase Horizontal FOV: l" << std::endl;

    std::cout << std::endl;
    std::cout << "Triangle debug info: LEFT_ALT" << std::endl;

}
