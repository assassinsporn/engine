
#include <SDL_surface.h>
#include "../headers/EngineBuffers.h"
#include "../headers/EngineSetup.h"
#include "../headers/Tools.h"
#include "../headers/LTimer.h"

EngineBuffers* EngineBuffers::instance = 0;

EngineBuffers* EngineBuffers::getInstance()
{
    if (instance == 0) {
        instance = new EngineBuffers();
    }

    return instance;
}

EngineBuffers::EngineBuffers()
{
    sizeBuffers = EngineSetup::getInstance()->SCREEN_WIDTH * EngineSetup::getInstance()->SCREEN_HEIGHT;

    std::cout << "EngineBuffers size: " << sizeBuffers << std::endl;

    depthBuffer = new float[sizeBuffers];
    videoBuffer = new Uint32[sizeBuffers];

}

void EngineBuffers::clearDepthBuffer()
{
    for (int i = 0 ; i < sizeBuffers ; i++) {
        depthBuffer[i] = NULL;
    }
}

float EngineBuffers::getDepthBuffer(int x, int y)
{
    return depthBuffer[(y * EngineSetup::getInstance()->SCREEN_WIDTH ) + x ];
}

float EngineBuffers::setDepthBuffer(int x, int y, float value)
{
    depthBuffer[(y * EngineSetup::getInstance()->SCREEN_WIDTH ) + x ] = value;
}




void EngineBuffers::setVideoBuffer(int x, int y, Uint32 value)
{
    int index = (y * EngineSetup::getInstance()->SCREEN_WIDTH ) + x;
    if (Tools::isPixelInWindow(x, y)) {
        videoBuffer[ index ] = value;
    } else {
        EngineBuffers::getInstance()->pixelesOutOfWindow++;
    }
}

float EngineBuffers::getVideoBuffer(int x, int y)
{
    return videoBuffer[(y * EngineSetup::getInstance()->SCREEN_WIDTH ) + x ];
}

void EngineBuffers::clearVideoBuffer()
{
    for (int i = 0 ; i < sizeBuffers ; i++) {
        videoBuffer[i] = Color::black();
    }
}

void EngineBuffers::flipVideoBuffer(SDL_Surface *surface, LightPoint *lp) {
    Uint32 *pixels = (Uint32 *)surface->pixels;

    for (int i = 0 ; i < sizeBuffers ; i++) {
        if (EngineSetup::getInstance()->SHOW_SHADOWMAPPING_BUFFER) {
            pixels[i] = Color::black() + lp->shadowMappingBuffer[i]*20;
        } else {
            pixels[i] = videoBuffer[i];
        }
    }
}

void EngineBuffers::resetBenchmarkValues() {
    t.start();

    EngineBuffers::getInstance()->pixelesDrawed = 0;
    EngineBuffers::getInstance()->pixelesOutOfWindow = 0;
    EngineBuffers::getInstance()->trianglesOutFrustum = 0;
    EngineBuffers::getInstance()->trianglesHidenByFaceCuling = 0;
    EngineBuffers::getInstance()->pixelesBehindOfCamera = 0;
    EngineBuffers::getInstance()->trianglesClippingCreated = 0;
}

void EngineBuffers::consoleInfo() {
    printf("Draw: %f  ", t.getTicks() / 1000.f);
    printf("| pxDrawed: %d ", EngineBuffers::getInstance()->pixelesDrawed);
    printf("| pxOutOfCamera: %d ", EngineBuffers::getInstance()->pixelesOutOfWindow);
    printf("| pxBehindOfCamera: %d ", EngineBuffers::getInstance()->pixelesBehindOfCamera);
    printf("| trOutFrustum: %d ", EngineBuffers::getInstance()->trianglesOutFrustum);
    printf("| trHByFaceCuling: %d ", EngineBuffers::getInstance()->trianglesHidenByFaceCuling);
    printf("| trClippingCreated: %d ", EngineBuffers::getInstance()->trianglesClippingCreated);
    printf("\r\n");
    t.stop();
}
