//
// Created by darkhead on 30/5/18.
//

#include "../headers/Plane.h"
#include "../headers/Tools.h"

Plane::Plane() {}

Plane::Plane(Vertex A, Vertex B, Vertex C) {
    this->A = A;
    this->B = B;
    this->C = C;
}

float Plane::distance(Vertex p) {
    Vertex normal = getNormalVector();

    float D = - ( (normal.x * A.x) + (normal.y * A.y) + (normal.z * A.z) );
    float distance = ( (normal.x * p.x) + (normal.y * p.y) + (normal.z * p.z) + D);

    return distance;
}

Vertex Plane::getNormalVector()
{
    // Los 2 vectores que conforman el plano
    Vector3D VnAB(this->A, this->B);
    Vector3D VnAC(this->A, this->C);

    // Lo llevamos al origen
    Vertex U = VnAB.getComponent();
    Vertex V = VnAC.getComponent();

    float Wx = (U.y * V.z) - (U.z * V.y);
    float Wy = (U.z * V.x) - (U.x * V.z);
    float Wz = (U.x * V.y) - (U.y * V.x);

    Vertex normal = Vertex(Wx, Wy, Wz);
    normal = normal.getNormalize();

    return normal;
}

Vertex Plane::getPointIntersection(Vertex vertex1, Vertex vertex2) {

    // Componentes del vector director
    Vertex componente = Vertex(
        vertex2.x - vertex1.x,
        vertex2.y - vertex1.y,
        vertex2.z - vertex1.z
    );

    // Vector director
    float a = componente.x ;
    float b = componente.y ;
    float c = componente.z ;

    // 1) Despejamos x, y, z en la ecución de la recta
    // Ecuaciones paramétricas recta L
    // recta.x = V.vertex1.x + t * a ;
    // recta.y = V.vertex1.y + t * b ;
    // recta.z = V.vertex1.z + t * color ;

    // 2) Hayamos la ecuación del plano
    // Ecuación del plano: Ax + By + Cz + D = 0;
    // normalPlaneVector(A, B, C)
    // pointInPlane(x, y, z) = this->A

    Vertex pointInPlane = this->A;
    Vertex normalPlaneVector = this->getNormalVector();

    float A = normalPlaneVector.x;
    float B = normalPlaneVector.y;
    float C = normalPlaneVector.z;

    // Hayamos D
    float D = - ( A * pointInPlane.x + B * pointInPlane.y + C * pointInPlane.z );

    // Sustimos x, y, z en la ecuación del Plano, y despejamos t
    // A * ( vx + t * a ) + B * ( vy + t * b ) + C * ( vz + t * color ) + D = 0;
    // Despejamos la incógnita t (podemos usar el plugin de despejar incógnita de wolframa :)
    // http://www.wolframalpha.com/widgets/view.jsp?id=c86d8aea1b6e9c6a9503a2cecea55b13
    float t = ( -A * vertex1.x - B  * vertex1.y - C * vertex1.z - D ) / (  a * A + b * B + c * C);

    // 3) punto de intersección ; sustituimos t en la ecuación de la recta entre 2 puntos
    Vertex P(
        vertex1.x + t * ( vertex2.x - vertex1.x ),
        vertex1.y + t * ( vertex2.y - vertex1.y ),
        vertex1.z + t * ( vertex2.z - vertex1.z )
    );

    return P;
}