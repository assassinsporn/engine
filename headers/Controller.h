//
// Created by darkhead on 28/4/18.
//

#ifndef SDL2_3D_ENGINE_CONTROLLER_H
#define SDL2_3D_ENGINE_CONTROLLER_H

#include "Camera.h"
#include "Mesh.h"
#include "Object3D.h"
#include <SDL.h>

class Controller {

    bool debug = true;
    bool click = false;

    int oldx, oldy;


public:
    void handleKeyboard(SDL_Event *, Camera *, bool &);

    Controller();

    void handleKeyboardObject3D(SDL_Event *event, Object3D *object);

    void handleKeyboarFrustum(SDL_Event *event, Frustum *f);
};


#endif //SDL2_3D_ENGINE_CONTROLLER_H
