//
// Created by darkhead on 29/4/18.
//

#ifndef SDL2_3D_ENGINE_ENGINE_H
#define SDL2_3D_ENGINE_ENGINE_H

#include <SDL.h>
#include "Controller.h"
#include "EngineSetup.h"
#include "LTimer.h"
#include "LightPoint.h"
#include <SDL_ttf.h>


class Engine {
public:

    SDL_Window *window;
    SDL_Surface *screenSurface;
    SDL_Texture *screenTexture;
    SDL_Renderer *renderer;

    SDL_Event e;

    Camera *cam;
    Controller *cont;

    Object3D **gameObjects;
    int numberGameObjects;

    LightPoint **lightPoints;
    int numberLightPoints;

    bool finish;

    TTF_Font *font = NULL;

    LTimer fpsTimer;

    Engine();

    void run();

    void Close();

    bool InitWindow();

    void InitFontsTTF();

    void onStart();

    void onUpdate();
    void onUpdateEvent();
    void onEnd();

    void drawMeshes();
    void drawLightPointsBillboard();
    void objects3DShadowMapping();
    void clearLightPointsShadowMappings();

    void addObject3D(Object3D *obj, std::string label);
    void addLightPoint(LightPoint *lightPoint, std::string label);

    Object3D* getObjectByLabel(std::string label);

    void mainLoop();

    void cameraUpdate();
    void windowUpdate();
};


#endif //SDL2_3D_ENGINE_ENGINE_H
