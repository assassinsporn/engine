#ifndef SDL2_3D_ENGINE_ENGINESETUP_H
#define SDL2_3D_ENGINE_ENGINESETUP_H

#include "Color.h"
#include "Vertex.h"

class EngineSetup {

public:
    EngineSetup();
    ~EngineSetup();

    static EngineSetup* getInstance();

    static EngineSetup* instance;
    const int ENGINE_MAX_GAMEOBJECTS = 100;

    // Screen dimension constants
    int SCREEN_WIDTH = 640;
    int SCREEN_HEIGHT = 480;

    // Draw axis
    bool RENDER_AXIS = true;
    bool RENDER_OBJECTS_AXIS = true;

    bool MESH_DEBUG_INFO = false;

    // Triangle render options
    // FaceCulling
    bool TRIANGLE_FACECULLING = true;
    bool TRIANGLE_FRUSTUM_CULLING = true;

    // Fill Triangle modes
    bool TRIANGLE_MODE_PIXELS = true;
    bool TRIANGLE_MODE_WIREFRAME = true;
    bool TRIANGLE_MODE_COLOR_SOLID = true;
    bool TRIANGLE_MODE_TEXTURIZED = true;

    // Triangle color for TRIANGLE_MODE_COLOR_SOLID
    Uint32 TRIANGLE_SOLID_TOP_COLOR = 0x00FF00;    // green
    Uint32 TRIANGLE_SOLID_BOTTOM_COLOR = 0x0000FF; // red

    // Show Extra Line Demo when triangle stripped in two when rasterizing
    bool TRIANGLE_DEMO_EXTRALINE_ENABLED = false;
    Uint32 TRIANGLE_DEMO_EXTRALINE = 0x00FF00; // blue

    // Normal in triangle
    bool TRIANGLE_RENDER_NORMAL = false;

    bool DRAW_OBJECT3D_BILLBOARD = true;

    float CONTROLLER_SPEED_MOVEMENT = 0.5f;
    float CONTROLLER_SPEED_ROTATION = 2.5f;

    float CONTROLLER_SPEED_ROTATION_CAMERA = 0.5f;
    float CONTROLLER_SPEED_MOVEMENT_CAMERA = 0.5f;

    // Z BUFFER
    bool TRIANGLE_RENDER_DEPTH_BUFFER = true;

    bool TRIANGLE_RENDER_CLIPPING = true;

    // FRUSTUM PLANES
    int NEAR_PLANE   = 0;

    // VIEW FRUSTUM
    int FAR_PLANE    = 1;
    int LEFT_PLANE   = 2;
    int RIGHT_PLANE  = 3;
    int TOP_PLANE    = 4;
    int BOTTOM_PLANE = 5;

    bool DRAW_FRUSTUM = false;

    float FRUSTUM_CLIPPING_DISTANCE = 0.0001f;

    Vertex CameraPosition = Vertex(1, 1, -15);

    std::string ICON_LIGHTPOINTS_DEFAULT = "../icons/lightpoint.tga";
    std::string ICON_OBJECT3D_DEFAULT    = "../icons/object3d.tga";

    bool SHOW_SHADOWMAPPING_BUFFER = false;

    void showHelp();
};


#endif //SDL2_3D_ENGINE_ENGINESETUP_H
