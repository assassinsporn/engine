#include "Game.h"
#include "headers/Drawable.h"
#include "headers/EngineBuffers.h"
#include "headers/M3.h"
#include "headers/Render.h"

void Game::run() {
    this->onStart();
    this->mainLoop();
    this->onEnd();
    Close();
}

void Game::onStart() {
    Engine::onStart();

    LightPoint *lp1 = new LightPoint();
    lp1->setLabel("lp1");
    lp1->setPosition(Vertex(1, 1, -5));
    lp1->color = Color::white();
    //lp1->setHandleKeyboard(true);
    //this->addLightPoint(lp1, "l1");

    LightPoint *lp2 = new LightPoint();
    lp2->setLabel("lp2");
    lp2->setPosition(Vertex(-3, 1, -1));
    lp2->color = Color::yellow();
    //lp2->setHandleKeyboard(true);
    //this->addLightPoint(lp2, "l2");


    // triangle
    Mesh *triangle = new Mesh();
    triangle->rotation.z+=180;
    triangle->rotation.x+=90;
    triangle->setLightPoints(Engine::lightPoints, Engine::numberLightPoints);
    triangle->setPosition( Vertex(1, 1, 5) );
    triangle->loadOBJBlender("../models/triangle_2uv.obj");
    triangle->setHandleKeyboard(true);
    this->addObject3D(triangle, "triangle");

    // cube
    Mesh *cubo = new Mesh();
    cubo->setLightPoints(Engine::lightPoints, Engine::numberLightPoints);
    cubo->scale = 20;
    cubo->setPosition( Vertex(1, 1, 15) );
    cubo->loadOBJBlender("../models/cubo.obj");
    //cubo->setHandleKeyboard(true);
    this->addObject3D(cubo, "cubo");

    // mono
    Mesh *mono = new Mesh();
    mono->setLightPoints(Engine::lightPoints, Engine::numberLightPoints);
    mono->setPosition( Vertex(1, 1, 5) );
    mono->loadOBJBlender("../models/mono.obj");
    mono->setHandleKeyboard(true);
    //this->addObject3D(mono, "mono");

    // q3 map
    //Mesh *q3map = new Mesh();
    //q3map->setLightPoints(Engine::lightPoints, Engine::numberLightPoints);
    //q3map->setPosition( Vertex(1, 1, 5) );
    //q3map->loadQ3Map("../pak0/maps/q3dm17.bsp");
    //q3map->setHandleKeyboard(true);
    //this->addObject3D(q3map, "q3map");

    //Billboard *cartel = new Billboard();
    //cartel->setPosition( Vertex(1, 1, 5) );
    //cartel->loadTexture("../pak0/icons/icona_rocket.tga");
    //this->addObject3D(cartel, "cartel");
}

void Game::mainLoop() {
    while(!finish) {
        while (SDL_PollEvent(&e)) {
            Engine::cameraUpdate();
            this->onUpdateEvent();
        }

        this->onUpdate();
        Engine::windowUpdate();
    }
}

void Game::onUpdateEvent() {
    Engine::onUpdateEvent();
}
void Game::onUpdate() {
    Engine::onUpdate();

    //Mesh *mono = (Mesh*) getObjectByLabel("mono");
}

void Game::onEnd() {
    Engine::onEnd();
}
